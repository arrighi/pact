[%%client.start]

open Eliom_content
open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt_js_events
open Diagram

type arrow_id = int * int * int

type mode = Arrow of arrow_id list | Object | Proof of int option * int option

module HashPath = struct
  type t = arrow_id list

  let equal =
    CCList.equal (fun (s1, t1, id1) (s2, t2, id2) ->
        Int.equal s1 s2 && Int.equal t1 t2 && Int.equal id1 id2)

  let hash = CCHash.list (CCHash.triple CCHash.int CCHash.int CCHash.int)
end

module Pathtbl = CCHashtbl.Make (HashPath)

let source : int ref = ref (-1)

let multi_select = ref false

let mode = ref Object

let next_id = ref 0

let paths_selection : arrow_id list list ref = CCList.Ref.create ()

let vertices = CCVector.create ()

let adj_list = CCVector.create ()

let proofs : (arrow_id list * string) list Pathtbl.t = Pathtbl.create 12

let all_paths s t =
  let seen = Array.make (CCVector.length vertices) false in
  let all_paths = ref [] in
  let rec aux u path =
    if u = t then all_paths := path :: !all_paths
    else (
      seen.(u) <- true;
      List.iter
        (fun arrow ->
          if not seen.(arrow.target) then
            aux arrow.target ((u, arrow.target, arrow.id) :: path))
        (CCVector.get adj_list u);
      seen.(u) <- false )
  in
  aux s [];
  !all_paths

let get_arrow (s, t, id) =
  List.find (fun a -> a.target = t && a.id = id) (CCVector.get adj_list s)

let unselect_arrow l =
  List.iter
    (fun (s, t, i) ->
      match
        Dom_html.getElementById_coerce
          (Printf.sprintf "%n %n %n" s t i)
          Dom_html.CoerceTo.div
      with
      | None -> Firebug.console##log (Js.string "Arrow not found")
      | Some a -> a##.classList##remove (Js.string "selected"))
    l

let add_class_arrow class_name l =
  List.iter
    (fun (s, t, i) ->
      match Dom_html.getElementById_opt (Printf.sprintf "%n %n %n" s t i) with
      | None -> Firebug.console##log (Js.string "Arrow not found")
      | Some elt -> elt##.classList##add (Js.string class_name))
    l

let highlight_arrow l =
  List.iter
    (fun (s, t, i) ->
      match
        Dom_html.getElementById_coerce
          (Printf.sprintf "%n %n %n" s t i)
          Dom_html.CoerceTo.div
      with
      | None -> Firebug.console##log (Js.string "Arrow not found")
      | Some a -> a##.classList##add (Js.string "selected"))
    l

let proof_to_li (color, p1, p2, value) =
  let p1 = List.rev p1 in
  let p2 = List.rev p2 in
  let f1 = List.hd p1 |> fun (s, _, _) -> s in
  let f2 = List.hd p2 |> fun (s, _, _) -> s in
  let open Html.D in
  li
    ~a:[ a_class [ Printf.sprintf "c%i-5" color ]; a_title value ]
    [
      txt
        ( String.concat " -> "
            ( (CCVector.get vertices f1).value
            :: List.map (fun (s, t, i) -> (CCVector.get vertices t).value) p1 )
        ^ " = "
        ^ String.concat " -> "
            ( (CCVector.get vertices f2).value
            :: List.map (fun (s, t, i) -> (CCVector.get vertices t).value) p2 )
        );
    ]

let path_to_li class_name path =
  let path = List.rev path in
  let first = List.hd path |> fun (s, _, _) -> s in
  let open Html.D in
  li
    ~a:[ a_class [ class_name ] ]
    [
      txt
        (String.concat " -> "
           ( (CCVector.get vertices first).value
           :: List.map (fun (s, t, i) -> (CCVector.get vertices t).value) path
           ));
    ]

let clusters_proofs paths =
  let i = ref 0 in
  let rec connected_component current paths =
    let neighbours, proof_list =
      Pathtbl.get_or ~default:[] proofs current
      |> List.filter_map (fun (path, proof) ->
             if List.mem path paths then Some (path, (!i, current, path, proof))
             else None)
      |> List.split
    in
    let paths =
      List.filter (fun path -> not (List.mem path neighbours)) paths
    in
    List.fold_left2
      (fun (acc_cc, acc_proofs, paths) n p ->
        let cluster, proof_list, paths = connected_component n paths in
        (cluster @ acc_cc, p :: (proof_list @ acc_proofs), paths))
      ([ current ], [], paths)
      neighbours proof_list
  in
  let rec aux = function
    | [] -> ([], [])
    | h :: t ->
        let cc, proof, paths = connected_component h t in
        incr i;
        let cluster, proof_list = aux paths in
        (cc :: cluster, proof @ proof_list)
  in
  aux paths

let unsetup_proof () =
  CCList.Ref.clear paths_selection;
  (Html.To_dom.of_ol ~%Toolbox.path)##querySelectorAll (Js.string "li")
  |> Dom.list_of_nodeList
  |> List.iter (Dom.removeChild (Html.To_dom.of_ol ~%Toolbox.path));
  CCList.Ref.clear paths_selection;
  (Html.To_dom.of_ol ~%Toolbox.proofs_list)##querySelectorAll (Js.string "li")
  |> Dom.list_of_nodeList
  |> List.iter (Dom.removeChild (Html.To_dom.of_ol ~%Toolbox.proofs_list));
  (Html.To_dom.of_section ~%Base.drawing_section)##querySelectorAll
    (Js.string ".grid-edge.selected")
  |> Dom.list_of_nodeList
  |> List.iter (fun arrow -> arrow##.classList##remove (Js.string "selected"));
  for i = 0 to 4 do
    (Html.To_dom.of_section ~%Base.drawing_section)##querySelectorAll
      (Js.string (Printf.sprintf ".grid-edge.c%i-5" i))
    |> Dom.list_of_nodeList
    |> List.iter (fun arrow ->
           arrow##.classList##remove (Js.string (Printf.sprintf "c%i-5" i)))
  done

let setup_proof () =
  match !mode with
  | Proof (Some s, Some t) ->
      CCList.Ref.clear paths_selection;
      let all_paths = all_paths s t in
      let clusters, proofs_list = clusters_proofs all_paths in
      List.iteri
        (fun i cluster ->
          List.iter
            (fun path ->
              add_class_arrow (Printf.sprintf "c%i-5" i) path;
              let li_path =
                path_to_li (Printf.sprintf "c%i-5" i) path |> Html.To_dom.of_li
              in
              Dom.appendChild (Html.To_dom.of_ol ~%Toolbox.path) li_path;
              Lwt_js_events.async (fun () ->
                  Lwt_js_events.clicks li_path (fun ev _ ->
                      Dom.preventDefault ev;
                      Dom_html.stopPropagation ev;
                      if CCList.Ref.lift (List.mem path) paths_selection then (
                        paths_selection :=
                          List.filter (( != ) path) !paths_selection;
                        li_path##.classList##remove (Js.string "selected");
                        if List.length !paths_selection < 2 then
                          (Html.To_dom.of_div ~%Toolbox.proof_form)##.classList##
                          remove
                            (Js.string "show") )
                      else (
                        if List.length !paths_selection = 1 then
                          (Html.To_dom.of_div ~%Toolbox.proof_form)##.classList##
                          add
                            (Js.string "show");
                        CCList.Ref.push paths_selection path;
                        li_path##.classList##add (Js.string "selected") );
                      Lwt.return_unit)))
            cluster)
        clusters;
      List.iter
        (fun proof ->
          let li_proof = proof_to_li proof |> Html.To_dom.of_li in
          Dom.appendChild (Html.To_dom.of_ol ~%Toolbox.proofs_list) li_proof)
        proofs_list
  | _ -> ()

let check_bt bt = (Html.To_dom.of_li bt)##.classList##add (Js.string "checked")

let uncheck_bt bt =
  (Html.To_dom.of_li bt)##.classList##remove (Js.string "checked")

let reset_arrow_toolbox () =
  uncheck_bt ~%Toolbox.bt_tail;
  uncheck_bt ~%Toolbox.bt_mapsto;
  uncheck_bt ~%Toolbox.bt_lhook;
  uncheck_bt ~%Toolbox.bt_rhook;
  uncheck_bt ~%Toolbox.bt_empty;
  uncheck_bt ~%Toolbox.bt_dotted;
  uncheck_bt ~%Toolbox.bt_dashed;
  uncheck_bt ~%Toolbox.bt_solid;
  uncheck_bt ~%Toolbox.bt_lharpoon;
  uncheck_bt ~%Toolbox.bt_rharpoon;
  uncheck_bt ~%Toolbox.bt_default_head;
  uncheck_bt ~%Toolbox.bt_two_head;
  uncheck_bt ~%Toolbox.bt_empty_head;
  uncheck_bt ~%Toolbox.bt_left_label;
  uncheck_bt ~%Toolbox.bt_inside_label;
  uncheck_bt ~%Toolbox.bt_right_label

let update_bt_state_arrow () =
  reset_arrow_toolbox ();
  match !mode with
  | Arrow (id :: l) ->
      let first = get_arrow id in
      let update value = function
        | None -> None
        | Some v -> if value = v then Some v else None
      in
      let fletching, arrowhead, label_position, shape =
        List.fold_left
          (fun (fletching, arrowhead, label_position, shape) id ->
            let arrow = get_arrow id in
            ( update arrow.fletching fletching,
              update arrow.arrowhead arrowhead,
              update arrow.label_position label_position,
              update arrow.shape shape ))
          ( Some first.fletching,
            Some first.arrowhead,
            Some first.label_position,
            Some first.shape )
          l
      in
      CCOpt.map_or ~default:()
        (fun f ->
          match f with
          | Straight -> check_bt ~%Toolbox.bt_empty
          | Tail -> check_bt ~%Toolbox.bt_tail
          | Mapsto -> check_bt ~%Toolbox.bt_mapsto
          | LHook -> check_bt ~%Toolbox.bt_lhook
          | RHook -> check_bt ~%Toolbox.bt_rhook)
        fletching;
      CCOpt.map_or ~default:()
        (fun ah ->
          match ah with
          | Empty -> check_bt ~%Toolbox.bt_empty_head
          | Single -> check_bt ~%Toolbox.bt_default_head
          | Double -> check_bt ~%Toolbox.bt_two_head
          | LHarpoon -> check_bt ~%Toolbox.bt_lharpoon
          | RHarpoon -> check_bt ~%Toolbox.bt_rharpoon)
        arrowhead;
      CCOpt.map_or ~default:()
        (fun label ->
          match label with
          | Inside -> check_bt ~%Toolbox.bt_inside_label
          | Left -> check_bt ~%Toolbox.bt_left_label
          | Right -> check_bt ~%Toolbox.bt_right_label)
        label_position;
      CCOpt.map_or ~default:()
        (fun shape ->
          match shape with
          | Dotted -> check_bt ~%Toolbox.bt_dotted
          | Dashed -> check_bt ~%Toolbox.bt_dashed
          | Solid -> check_bt ~%Toolbox.bt_solid)
        shape
  | _ -> ()

let highlight_vertex id =
  match
    Dom_html.getElementById_coerce (string_of_int id) Dom_html.CoerceTo.div
  with
  | None -> Firebug.console##log (Js.string "Vertex not found")
  | Some a -> a##.classList##add (Js.string "selected")

let unhighlight_vertex id =
  match
    Dom_html.getElementById_coerce (string_of_int id) Dom_html.CoerceTo.div
  with
  | None -> Firebug.console##log (Js.string "Vertex not found")
  | Some a -> a##.classList##remove (Js.string "selected")

let select_arrow arrow_list id arrow =
  arrow##.classList##add (Js.string "selected");
  (Html.To_dom.of_section ~%Toolbox.arrow)##.className := Js.string "show";
  if !multi_select then mode := Arrow (id :: arrow_list)
  else (
    unselect_arrow arrow_list;
    mode := Arrow [ id ] );
  update_bt_state_arrow ()

let select_source source =
  match Dom_html.getElementById_coerce "source" Dom_html.CoerceTo.div with
  | None -> Firebug.console##log (Js.string "Source div not found")
  | Some s -> s##.innerHTML := Js.string (CCVector.get vertices source).value

let select_target target =
  match Dom_html.getElementById_coerce "target" Dom_html.CoerceTo.div with
  | None -> Firebug.console##log (Js.string "Target div not found")
  | Some t -> t##.innerHTML := Js.string (CCVector.get vertices target).value

let unselect_source () =
  match Dom_html.getElementById_coerce "source" Dom_html.CoerceTo.div with
  | None -> Firebug.console##log (Js.string "Source div not found")
  | Some s -> s##.innerHTML := Js.string "Select Source"

let unselect_target () =
  match Dom_html.getElementById_coerce "target" Dom_html.CoerceTo.div with
  | None -> Firebug.console##log (Js.string "Target div not found")
  | Some t -> t##.innerHTML := Js.string "Select Target"

let add_arrow target_elt arrow =
  let arrow = Html.To_dom.of_div arrow in
  let source, target, id =
    match String.split_on_char ' ' (Js.to_string arrow##.id) with
    | [ source; target; id ] ->
        (int_of_string source, int_of_string target, int_of_string id)
    | _ -> assert false
  in
  Dom.appendChild (Html.To_dom.of_section target_elt) arrow;
  Lwt_js_events.async (fun () ->
      Lwt_js_events.clicks arrow (fun ev _ ->
          match !mode with
          | Arrow l ->
              Dom.preventDefault ev;
              Dom_html.stopPropagation ev;
              if List.mem (source, target, id) l then (
                let new_list =
                  List.filter (fun a -> a <> (source, target, id)) l
                in
                mode := Arrow new_list;
                arrow##.classList##remove (Js.string "selected");
                if new_list = [] then
                  (Html.To_dom.of_section ~%Toolbox.arrow)##.className
                  := Js.string ""
                else update_bt_state_arrow () )
              else select_arrow l (source, target, id) arrow;
              Lwt.return_unit
          | _ -> Lwt.return_unit))

let remove_by_id id =
  Dom.removeChild
    (Html.To_dom.of_section ~%Base.drawing_section)
    (Dom_html.getElementById id)

let remove_arrow ((source, target, id) as arrow_id) =
  remove_by_id (Printf.sprintf "%i %i %i" source target id);
  Pathtbl.filter_map_inplace
    (fun p l ->
      if List.mem arrow_id p then None
      else
        let new_l = List.filter (fun (p, _) -> not (List.mem arrow_id p)) l in
        if l = [] then None else Some new_l)
    proofs

let remove_vertex id =
  CCVector.set vertices id { (CCVector.get vertices id) with id = -1 };
  CCVector.map_in_place
    (List.filter (fun arrow ->
         if arrow.source = id || arrow.target = id then (
           remove_arrow (arrow.source, arrow.target, arrow.id);
           false )
         else true))
    adj_list;
  remove_by_id (string_of_int id)

let add_vertex vertices adj_list target
    (vertex, value_form, value_input, value_div, deleter) =
  let vertex = Html.To_dom.of_div vertex in
  let value_form = Html.To_dom.of_form value_form in
  let value_input = Html.To_dom.of_input value_input in
  let value_div = Html.To_dom.of_div value_div in
  let deleter = Html.To_dom.of_img deleter in
  Dom.appendChild (Html.To_dom.of_section target) vertex;
  value_input##focus;
  Lwt_js_events.async (fun () ->
      Lwt_js_events.keydowns value_input (fun ev _ ->
          if ev##.keyCode = 13 then (
            Dom.preventDefault ev;
            value_input##blur );
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups vertex (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          match !mode with
          | Arrow l ->
              let id = Js.parseInt vertex##.id in
              if id <> !source && !source <> -1 then (
                let arrow =
                  Diagram.add_arrow vertices adj_list !next_id !source id
                in
                select_arrow l (!source, id, !next_id)
                  (Html.To_dom.of_div arrow);
                incr next_id;
                add_arrow target arrow;
                source := -1 )
              else source := -1;
              Lwt.return_unit
          | Proof (source, target) ->
              let id = Js.parseInt vertex##.id in
              ( match (source, target) with
              | None, None ->
                  mode := Proof (Some id, None);
                  select_source id;
                  highlight_vertex id
              | None, Some t ->
                  if id = t then (
                    mode := Proof (None, None);
                    unselect_target ();
                    unhighlight_vertex id )
                  else (
                    mode := Proof (Some id, Some t);
                    setup_proof ();
                    select_source id;
                    highlight_vertex id )
              | Some s, None ->
                  if id = s then (
                    mode := Proof (None, None);
                    unselect_source ();
                    unhighlight_vertex id )
                  else (
                    mode := Proof (Some s, Some id);
                    setup_proof ();
                    select_target id;
                    highlight_vertex id )
              | Some s, Some t ->
                  unsetup_proof ();
                  if id = t then (
                    mode := Proof (Some s, None);
                    unselect_target ();
                    unhighlight_vertex id )
                  else if id = s then (
                    mode := Proof (None, Some t);
                    unselect_source ();
                    unhighlight_vertex id )
                  else (
                    mode := Proof (Some id, None);
                    select_source id;
                    unhighlight_vertex s;
                    unhighlight_vertex t;
                    highlight_vertex id ) );
              Lwt.return_unit
          | Object ->
              value_input##.value := value_div##.innerHTML;
              value_form##.classList##remove (Js.string "hide");
              vertex##.classList##add (Js.string "edit");
              value_input##focus;
              Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.blurs value_input (fun _ _ ->
          value_form##.classList##add (Js.string "hide");
          vertex##.classList##remove (Js.string "edit");
          let value = value_input##.value in
          let v = CCVector.get vertices (Js.parseInt vertex##.id) in
          v.value <- Js.to_string value;
          value_div##.innerHTML := value;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.clicks deleter (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          let id = Js.parseInt vertex##.id in
          remove_vertex id;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mousedowns vertex (fun ev _ ->
          match !mode with
          | Arrow _ ->
              Dom.preventDefault ev;
              Dom_html.stopPropagation ev;
              source := Js.parseInt vertex##.id;
              Lwt.return_unit
          | _ -> Lwt.return_unit))

let update_fletching shape =
  match !mode with
  | Arrow l ->
      List.iter
        (fun ((s, t, i) as id) ->
          let arrow = get_arrow id in
          arrow.fletching <- shape;
          (* TODO: voir avec Drup, patcher jsoo pour avoir querySelector dans Dom_svg *)
          let arrow_div =
            Dom_html.getElementById (Printf.sprintf "%i %i %i" s t i)
          in
          let tail =
            arrow_div##querySelector (Js.string "image.tail") |> fun tail ->
            Js.Opt.bind tail Dom_svg.CoerceTo.element |> fun tail ->
            Js.Opt.bind tail Dom_svg.CoerceTo.image
          in
          Js.Opt.case tail
            (fun () ->
              Firebug.console##log (Js.string "Fail to retrive the tail image"))
            (fun tail ->
              tail##.href##.baseVal := Js.string (fletching_to_file shape)))
        l;
      update_bt_state_arrow ()
  | _ -> ()

let update_arrowhead shape =
  match !mode with
  | Arrow l ->
      List.iter
        (fun ((s, t, i) as id) ->
          let arrow = get_arrow id in
          arrow.arrowhead <- shape;
          (* TODO: voir avec Drup, patcher jsoo pour avoir querySelector dans Dom_svg *)
          let arrow_div =
            Dom_html.getElementById (Printf.sprintf "%i %i %i" s t i)
          in
          let head =
            arrow_div##querySelector (Js.string "image.head") |> fun head ->
            Js.Opt.bind head Dom_svg.CoerceTo.element |> fun head ->
            Js.Opt.bind head Dom_svg.CoerceTo.image
          in
          Js.Opt.case head
            (fun () ->
              Firebug.console##log (Js.string "Fail to retrive the head image"))
            (fun head ->
              head##.href##.baseVal := Js.string (arrowhead_to_file shape)))
        l;
      update_bt_state_arrow ()
  | _ -> ()

let update_arrow_shape shape =
  match !mode with
  | Arrow l ->
      List.iter
        (fun ((s, t, i) as id) ->
          let arrow = get_arrow id in
          arrow.shape <- shape;
          (* TODO: voir avec Drup, patcher jsoo pour avoir querySelector dans Dom_svg *)
          let arrow_div =
            Dom_html.getElementById (Printf.sprintf "%i %i %i" s t i)
          in
          let core =
            arrow_div##querySelector (Js.string "path.core") |> fun core ->
            Js.Opt.bind core Dom_svg.CoerceTo.element |> fun core ->
            Js.Opt.bind core Dom_svg.CoerceTo.path
          in
          let string_value =
            arrow_shape_to_value shape
            |> List.map (fun (f, _) -> int_of_float f)
            |> List.map string_of_int |> String.concat " "
          in
          Js.Opt.case core
            (fun () ->
              Firebug.console##log (Js.string "Fail to retrive the core path"))
            (fun core ->
              core##setAttribute
                (Js.string "stroke-dasharray")
                (Js.string string_value)))
        l;
      update_bt_state_arrow ()
  | _ -> ()

let update_arrow_bend bend =
  match !mode with
  | Arrow l ->
      List.iter
        (fun ((s, t, i) as id) ->
          let arrow = get_arrow id in
          arrow.bend <- arrow.bend + bend;
          let arrow_div =
            Dom_html.getElementById (Printf.sprintf "%i %i %i" s t i)
          in
          let new_arrow = arrow_to_html vertices arrow in
          Dom.removeChild
            (Html.To_dom.of_section ~%Base.drawing_section)
            arrow_div;
          (Html.To_dom.of_div new_arrow)##.classList##add (Js.string "selected");
          add_arrow ~%Base.drawing_section new_arrow)
        l
  | _ -> ()

let init_arrow_toolbox () =
  (* Arrow Fletching button *)
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_tail) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_fletching Tail;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_mapsto)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_fletching Mapsto;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_lhook) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_fletching LHook;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_rhook) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_fletching RHook;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_empty) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_fletching Straight;
          Lwt.return_unit));

  (* Arrow Head button *)
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_lharpoon)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrowhead LHarpoon;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_rharpoon)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrowhead RHarpoon;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_default_head)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrowhead Single;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_two_head)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrowhead Double;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_empty_head)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrowhead Empty;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_dotted)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrow_shape Dotted;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_dashed)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrow_shape Dashed;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_solid) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrow_shape Solid;
          Lwt.return_unit));
  (* Arrow bend button *)
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_left) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrow_bend (-1);
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_right) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          update_arrow_bend 1;
          Lwt.return_unit));
  (* Arrow remove button *)
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_li ~%Toolbox.bt_remove)
        (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          ( match !mode with
          | Arrow l ->
              List.iter
                (fun ((source, target, id) as arrow_id) ->
                  remove_arrow arrow_id;
                  CCVector.set adj_list source
                    (List.filter
                       (fun arrow -> arrow.target != target || arrow.id != id)
                       (CCVector.get adj_list source)))
                l;
              (Html.To_dom.of_section ~%Toolbox.arrow)##.className
              := Js.string ""
          | _ -> () );
          Lwt.return_unit));

  (* Multi select with ctrl *)
  Lwt_js_events.async (fun () ->
      Lwt_js_events.keydowns Dom_html.document (fun ev _ ->
          if ev##.keyCode = 17 then multi_select := true;
          Lwt.return_unit));
  Lwt_js_events.async (fun () ->
      Lwt_js_events.keyups Dom_html.document (fun ev _ ->
          if ev##.keyCode = 17 then multi_select := false;
          Lwt.return_unit))

let init () =
  init_arrow_toolbox ();
  let vertices_div, arrows_div = diagram_to_html (vertices, adj_list) in
  CCVector.iter
    (fun vertex -> add_vertex vertices adj_list ~%Base.drawing_section vertex)
    vertices_div;
  CCVector.iter
    (fun l -> List.iter (fun arrow -> add_arrow ~%Base.drawing_section arrow) l)
    arrows_div;
  Lwt_js_events.async (fun () ->
      Lwt_js_events.mouseups (Html.To_dom.of_section ~%Base.drawing_section)
        (fun ev _ ->
          Dom.preventDefault ev;
          match !mode with
          | Object ->
              let new_vertex =
                Diagram.add_vertex vertices adj_list
                  (float ev##.clientX)
                  (float ev##.clientY)
              in
              add_vertex vertices adj_list ~%Base.drawing_section new_vertex;
              Lwt.return_unit
          | _ -> Lwt.return_unit));
  let bt_object = Html.To_dom.of_li ~%Toolbox.bt_object in
  let bt_arrow = Html.To_dom.of_li ~%Toolbox.bt_arrow in
  let bt_proof = Html.To_dom.of_li ~%Toolbox.bt_proof in
  let drawing_section = Html.To_dom.of_section ~%Base.drawing_section in
  async (fun () ->
      clicks bt_object (fun ev _ ->
          drawing_section##.className := Js.string "pan";
          match !mode with
          | Arrow l ->
              unselect_arrow l;
              (Html.To_dom.of_section ~%Toolbox.arrow)##.className
              := Js.string "";
              mode := Object;
              bt_object##.classList##add (Js.string "checked");
              bt_arrow##.classList##remove (Js.string "checked");
              Lwt.return_unit
          | Proof (s, t) ->
              unselect_source ();
              unselect_target ();
              Option.iter unhighlight_vertex s;
              Option.iter unhighlight_vertex t;
              unsetup_proof ();
              (Html.To_dom.of_section ~%Toolbox.proof)##.classList##remove
                (Js.string "show");
              mode := Object;
              bt_object##.classList##add (Js.string "checked");
              bt_proof##.classList##remove (Js.string "checked");
              Lwt.return_unit
          | Object -> Lwt.return_unit));
  async (fun () ->
      clicks bt_arrow (fun ev _ ->
          drawing_section##.className := Js.string "arrow";
          match !mode with
          | Object ->
              mode := Arrow [];
              bt_arrow##.classList##add (Js.string "checked");
              bt_object##.classList##remove (Js.string "checked");
              Lwt.return_unit
          | Proof (s, t) ->
              unselect_source ();
              unselect_target ();
              Option.iter unhighlight_vertex s;
              Option.iter unhighlight_vertex t;
              unsetup_proof ();
              (Html.To_dom.of_section ~%Toolbox.proof)##.classList##remove
                (Js.string "show");
              mode := Arrow [];
              bt_arrow##.classList##add (Js.string "checked");
              bt_proof##.classList##remove (Js.string "checked");
              Lwt.return_unit
          | Arrow l ->
              unselect_arrow l;
              mode := Arrow [];
              Lwt.return_unit));
  async (fun () ->
      clicks bt_proof (fun ev _ ->
          drawing_section##.className := Js.string "proofs";
          match !mode with
          | Arrow l ->
              unselect_arrow l;
              (Html.To_dom.of_section ~%Toolbox.arrow)##.className
              := Js.string "";
              (Html.To_dom.of_section ~%Toolbox.proof)##.classList##add
                (Js.string "show");
              mode := Proof (None, None);
              bt_proof##.classList##add (Js.string "checked");
              bt_arrow##.classList##remove (Js.string "checked");
              Lwt.return_unit
          | Proof _ -> Lwt.return_unit
          | Object ->
              (Html.To_dom.of_section ~%Toolbox.proof)##.classList##add
                (Js.string "show");
              mode := Proof (None, None);
              bt_proof##.classList##add (Js.string "checked");
              bt_object##.classList##remove (Js.string "checked");
              Lwt.return_unit));
  async (fun () ->
      clicks (Html.To_dom.of_button ~%Toolbox.submit) (fun ev _ ->
          Dom.preventDefault ev;
          Dom_html.stopPropagation ev;
          let value =
            Js.to_string (Html.To_dom.of_textarea ~%Toolbox.text_proof)##.value
          in
          List.iter
            (fun path1 ->
              List.iter
                (fun path2 ->
                  if path1 != path2 then
                    Pathtbl.update proofs
                      (fun _ l ->
                        match l with
                        | None -> Some [ (path2, value) ]
                        | Some l -> Some ((path2, value) :: l))
                      path1)
                !paths_selection)
            !paths_selection;
          (Html.To_dom.of_div ~%Toolbox.proof_form)##.classList##remove
            (Js.string "show");
          unsetup_proof ();
          setup_proof ();
          Lwt.return_unit))
