[%%shared.start]

open Eliom_content

type vertex = { id : int; mutable value : string; position : float * float }

type arrow_shape = Dotted | Dashed | Solid

type fletching = Straight | Tail | Mapsto | LHook | RHook

type arrowhead = Empty | Single | Double | LHarpoon | RHarpoon

type position = Inside | Left | Right

type bend = int (*Negative bend left positive bend right*)

type arrow = {
  id : int;
  source : int;
  target : int;
  mutable fletching : fletching;
  (*Start of the arrow*)
  mutable arrowhead : arrowhead;
  (*End of the arrow*)
  mutable label : string;
  mutable label_position : position;
  mutable bend : bend;
  mutable shape : arrow_shape;
}

type diagram = vertex array * arrow list array

let vertex_to_html { id; value; position = x, y } =
  let h = 50. and w = 70. in
  let open Html.D in
  let value_input =
    input ~a:[ a_input_type `Text; a_name "value"; a_value value ] ()
  in
  let deleter =
    img
      ~a:[ a_class [ "deleter" ] ]
      ~src:
        (make_uri
           (Eliom_service.static_dir ())
           [ "img"; "properties"; "trash.svg" ])
      ~alt:"Remove" ()
  in
  let value_form = form ~a:[ a_class [ "edit" ] ] [ value_input ] in
  let value_div =
    div
      ~a:
        [
          a_class [ "value" ];
          a_style (Printf.sprintf "width: %fpx; margin: 0 auto;" w);
        ]
      [ txt value ]
  in
  ( div
      ~a:
        [
          a_style
            (Printf.sprintf
               "height: %fpx; width: %fpx; left: %fpx; top: %fpx; line-height: \
                %fpx;"
               h w
               (x -. (w /. 2.))
               (y -. (h /. 2.))
               h);
          a_class [ "grid-cell"; "edit" ];
          a_id (Printf.sprintf "%n" id);
          a_draggable true;
        ]
      [ deleter; value_div; value_form ],
    value_form,
    value_input,
    value_div, deleter )

let distance (sx, sy) (tx, ty) = sqrt (((sx -. tx) ** 2.) +. ((sy -. ty) ** 2.))

let arrow_shape_to_value = function
  | Dotted -> [ (2., None); (4., None) ]
  | Dashed -> [ (7., None); (3., None) ]
  | Solid -> []

let fletching_to_file fletching =
  "./img/arrow/"
  ^
  match fletching with
  | Straight -> "none.svg"
  | Tail -> "tail.svg"
  | Mapsto -> "mapsto.svg"
  | LHook -> "hook.svg"
  | RHook -> "hookalt.svg"

let arrowhead_to_file head =
  "./img/arrow/"
  ^
  match head with
  | Empty -> "none.svg"
  | Single -> "default.svg"
  | Double -> "twoheads.svg"
  | LHarpoon -> "harpoon.svg"
  | RHarpoon -> "harpoonalt.svg"

let arrow_to_html vertices
    {
      id;
      source;
      target;
      fletching;
      arrowhead;
      label;
      label_position;
      bend;
      shape;
    } =
  let { id = _; value = _; position = sp } = CCVector.get vertices source in
  let { id = _; value = _; position = tp } = CCVector.get vertices target in
  let point_angle = atan ((snd tp -. snd sp) /. (fst tp -. fst sp)) in
  let point_angle =
    if fst sp < fst tp then point_angle else point_angle -. acos (-1.)
  in
  let bend_angle = float bend *. 1.57079632679489656 /. 4. in
  let bend_angle_degree = float bend *. 90. /. 4. in
  let start_left = fst sp +. (20. *. cos (point_angle +. bend_angle)) in
  let start_top = snd sp +. (20. *. sin (point_angle +. bend_angle)) in
  let end_left =
    fst tp +. (20. *. cos (point_angle -. bend_angle -. acos (-1.)))
  in
  let end_top =
    snd tp +. (20. *. sin (point_angle -. bend_angle -. acos (-1.)))
  in
  let length = distance (start_left, start_top) (end_left, end_top) +. 6.5 in
  let rotation_angle =
    atan ((end_top -. start_top) /. (end_left -. start_left))
  in
  let rotation_angle =
    if fst sp < fst tp then rotation_angle else rotation_angle -. acos (-1.)
  in
  let controle_height = (length -. 6.5) /. 2. *. tan bend_angle in
  let height = (Float.abs controle_height /. 2.) +. 13. in
  let start_height = if bend < 0 then height -. 6.5 else 6.5 in
  let start_width = 6.5 in
  let core_arrow =
    let open Svg.D in
    path
      ~a:
        [
          a_d
            (Printf.sprintf "M %f %f q %f %f %f 0" start_width start_height
               (length /. 2.) controle_height (length -. 6.5));
          a_stroke (`Color ("black", None));
          a_fill (`Color ("transparent", None));
          a_class [ "core" ];
          a_stroke_dasharray (arrow_shape_to_value shape);
        ]
      []
  in
  let selection =
    let open Svg.D in
    path
      ~a:
        [
          a_d
            (Printf.sprintf "M %f %f q %f %f %f 0" start_width start_height
               (length /. 2.) controle_height (length -. 6.5));
          a_stroke (`Color ("transparent", None));
          a_fill (`Color ("none", None));
          a_stroke_linecap `Square;
          a_stroke_width (12., None);
          a_class [ "mouse" ];
        ]
      []
  in
  let tail =
    let open Svg.D in
    image
      ~a:
        [
          a_x (0., None);
          a_y (start_height -. 6.5, None);
          a_width (6.5, None);
          a_height (13., None);
          a_href (fletching_to_file fletching);
          a_transform
            [ `Rotate ((bend_angle_degree, None), Some (6.5, start_height)) ];
          a_class [ "tail" ];
        ]
      []
  in
  let head =
    let open Svg.D in
    image
      ~a:
        [
          a_x (length -. 6.5, None);
          a_y (start_height -. 6.5, None);
          a_width (6.5, None);
          a_height (13., None);
          a_href (arrowhead_to_file arrowhead);
          a_transform
            [
              `Rotate ((-.bend_angle_degree, None), Some (length, start_height));
            ];
          a_class [ "head" ];
        ]
      []
  in
  Html.D.div
    ~a:
      [
        Html.D.a_style
          (Printf.sprintf
             "border: 0px; width: %fpx; height: %fpx; left: %fpx; top: %fpx; \
              transform-origin:%fpx %fpx; transform: rotate(%frad)"
             (length +. 6.5) height
             (start_left -. start_width)
             (start_top -. start_height)
             start_width start_height rotation_angle);
        Html.D.a_class [ "grid-edge" ];
        Html.D.a_id (Printf.sprintf "%n %n %n" source target id);
      ]
    [
      (let open Svg.D in
      Html.D.svg
        ~a:[ a_width (length +. 6.5, None); a_height (height, None) ]
        [ selection; core_arrow; tail; head ]);
    ]

let diagram_to_html (vertices, adj_list) =
  ( CCVector.map vertex_to_html vertices,
    CCVector.map (fun l -> List.map (arrow_to_html vertices) l) adj_list )

let add_vertex vertices adj_list ?(value = "") x y =
  let vertex = { id = CCVector.length vertices; value; position = (x, y) } in
  CCVector.push vertices vertex;
  CCVector.push adj_list [];
  vertex_to_html vertex

let add_arrow vertices adj_list ?(fletching = Straight) ?(arrowhead = Single)
    ?(label = "") ?(label_position = Left) ?(bend = 0) ?(shape = Solid) id
    source target =
  let arrow =
    {
      id;
      source;
      target;
      fletching;
      arrowhead;
      label;
      label_position;
      bend;
      shape;
    }
  in
  CCVector.set adj_list source (arrow :: CCVector.get adj_list source);
  arrow_to_html vertices arrow
