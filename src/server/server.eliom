
module Pact_app = Eliom_registration.App (struct
  let application_name = "pact"

  let global_data_path = None
end)

let main_service =
  Eliom_service.create ~path:(Eliom_service.Path [])
    ~meth:(Eliom_service.Get (Eliom_parameter.unit)) ()

let name_service =
  Eliom_service.create ~path:(Eliom_service.Path [])
    ~meth:(Eliom_service.Get (Eliom_parameter.string "name")) ()
