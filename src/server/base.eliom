open Eliom_content

let drawing_section =
  let open Html.D in
  section ~a:[a_id "grid"; a_class ["pan"]] [txt ""]

let%server page () =
  let open Html.D in
  html
    (head
       (title (txt "Pact"))
       [
         css_link
           ~uri:(make_uri (Eliom_service.static_dir ()) [ "css"; "pact.css" ])
           ();
       ])
    (body [ Toolbox.proof; Toolbox.arrow; drawing_section; Toolbox.main ])
