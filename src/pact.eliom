[%%shared open Eliom_content]

[%%shared open Diagram]

open Server

let () =
  Pact_app.register ~service:main_service (fun () () ->
      let page = Base.page () in
      let _ =
        [%client
          ( Client.init ()
            : unit )]
      in
      Lwt.return page);
  Pact_app.register ~service:name_service (fun name () ->
      let page = Base.page () in
      let _ =
        [%client
          ( Client.init ()
            : unit )]
      in
      Lwt.return page)
