open Eliom_content

let dummy_link elt =
  Html.D.Raw.(a ~a:[ a_href (Xml.uri_of_string "#") ]) [ elt ]

let image url alt =
  Html.D.(
    img
      ~src:
        (make_uri
           ~service:(Eliom_service.static_dir ())
           [ "img"; "tools"; "blank.svg" ])
      ~alt
      ~a:[ a_style ("background-image: url(\"" ^ url ^ "\");") ]
      ())

let separator =
  let open Html.F in
  li ~a:[a_class ["separator"]] [txt "Separator"]

let bt_object =
  Html.D.(
    li
      ~a:[ a_class [ "button"; "checked" ]; a_title "Pan Tool" ]
      [ dummy_link (image "./img/tools/pan.svg" "Pan Tool") ])

let bt_arrow =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Arrow Tool" ]
      [ dummy_link (image "./img/tools/arrow.svg" "Arrow Tool") ])

let bt_proof =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Proof Tool" ]
      [ dummy_link (image "./img/tools/proof.svg" "Proof Tool") ])

let bt_undo =
  Html.D.(
    li
      ~a:[ a_class [ "button"; "disabled" ]; a_title "Undo" ]
      [ dummy_link (image "./img/tools/undo.svg" "Undo") ])

let bt_redo =
  Html.D.(
    li
      ~a:[ a_class [ "button"; "disabled" ]; a_title "Redo" ]
      [ dummy_link (image "./img/tools/redo.svg" "Redo") ])

let bt_open_code =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Open Code" ]
      [ dummy_link (image "./img/tools/code.svg" "Open Code") ])

let bt_permalink =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Copy Diagram Permalink" ]
      [ dummy_link (image "./img/tools/link.svg" "Copy Diagram Permalink") ])

let bt_repository =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Git Repository" ]
      [
        Html.D.(
          a
            (Eliom_service.extern ~prefix:"https://gitlab.crans.org/"
               ~path:[ "arrighi"; "pact" ]
               ~meth:(Eliom_service.Get Eliom_parameter.unit) ())
            [ image "./img/tools/about.svg" "Git Repository" ]
            ());
      ])

let main =
  let open Html.D in
  section ~a:[a_id "toolbox"; a_class ["toolbox"]] [ul
      [
        bt_object;
        bt_arrow;
        bt_proof;
        separator;
        (*bt_undo;
        bt_redo;
        separator;
        bt_open_code;
        bt_permalink;
        separator;*)
        bt_repository;
      ]]

let bt_reverse =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Reverse Arrow" ]
      [ dummy_link (image "./img/properties/reverse.svg" "Reverse Arrow") ])

let bt_tail =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Tail" ]
      [ dummy_link (image "./img/properties/tail.svg" "Tail") ])

let bt_mapsto =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Maps To" ]
      [ dummy_link (image "./img/properties/mapsto.svg" "Maps To") ])

let bt_lhook =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Left Hook" ]
      [ dummy_link (image "./img/properties/hook.svg" "Left Hook") ])

let bt_rhook =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Right Hook" ]
      [ dummy_link (image "./img/properties/hookalt.svg" "Right Hook") ])

let bt_empty =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Empty" ]
      [ dummy_link (image "./img/properties/empty.svg" "Empty") ])

let bt_right =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Bend Right (Down Arrow)" ]
      [
        dummy_link
          (image "./img/properties/bendright.svg" "Bend Right");
      ])

let bt_dotted =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Dotted" ]
      [ dummy_link (image "./img/properties/dotted.svg" "Dotted") ])

let bt_dashed =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Dashed" ]
      [ dummy_link (image "./img/properties/dashed.svg" "Dashed") ])

let bt_solid =
  Html.D.(
    li
      ~a:[ a_class [ "button"; "checked" ]; a_title "Solid" ]
      [ dummy_link (image "./img/properties/solid.svg" "Solid") ])

let bt_left =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Bend Left" ]
      [
        dummy_link
          (image "./img/properties/bendleft.svg" "Bend Left");
      ])

let bt_lharpoon =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Left Harpoon" ]
      [ dummy_link (image "./img/properties/harpoon.svg" "Left Harpoon") ])

let bt_rharpoon =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Right Harpoon" ]
      [ dummy_link (image "./img/properties/harpoonalt.svg" "Right Harpoon") ])

let bt_default_head =
  Html.D.(
    li
      ~a:[ a_class [ "button"; "checked" ]; a_title "Default Head" ]
      [ dummy_link (image "./img/properties/head.svg" "Default Head") ])

let bt_two_head =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Two Head" ]
      [ dummy_link (image "./img/properties/twoheads.svg" "Two Heads") ])

let bt_empty_head =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Empty Head" ]
      [ dummy_link (image "./img/properties/empty.svg" "Empty Head") ])

let bt_two_head =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Two Head" ]
      [ dummy_link (image "./img/properties/twoheads.svg" "Two Heads") ])

let bt_left_label =
  Html.D.(
    li
      ~a:[ a_class [ "button"; "checked" ]; a_title "Left Label" ]
      [ dummy_link (image "./img/properties/labelleft.svg" "Left Label") ])

let bt_inside_label =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Inside Label" ]
      [
        dummy_link (image "./img/properties/labelinside.svg" "Inside Label");
      ])

let bt_right_label =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Right Label" ]
      [ dummy_link (image "./img/properties/labelright.svg" "Right Label") ])

let bt_edit_label =
  Html.D.(
    li
      ~a:[ a_class [ "button" ]; a_title "Edit Label" ]
      [ dummy_link (image "./img/properties/edit.svg" "Edit Label") ])

let bt_remove =
  Html.D.(
    li
      ~a:[ a_class [ "button"; "remove" ]; a_title "Remove Arrow" ]
      [ dummy_link (image "./img/properties/trash.svg" "Remove Arrow") ])

let arrow =
  let open Html.D in
  section ~a:[a_id "properties"]
  [section ~a:[a_class ["toolbox"]] [ul [
        (*bt_reverse;
        separator;*)
        bt_tail;
        bt_mapsto;
        bt_lhook;
        bt_rhook;
        bt_empty;
        separator;
        bt_right;
        bt_dotted;
        bt_dashed;
        bt_solid;
        bt_left;
        separator;
        bt_lharpoon;
        bt_rharpoon;
        bt_default_head;
        bt_two_head;
        bt_empty_head;
        (*separator;
        bt_left_label;
        bt_inside_label;
        bt_right_label;
        bt_edit_label;*)
        separator;
        bt_remove;
      ]

                                    ];
  form ~a:[a_class ["edit"]; a_style "left: 0px; top: 0px;"]
    [input ~a:[a_input_type `Text] ()]]

let path =
  let open Html.D in
  ol ~a:[ a_id "path" ] []

let submit =
  let open Html.D.Raw in
  button ~a:[ a_button_type `Button ] [ txt "Submit" ]

let text_proof =
  let open Html.D.Raw in
  textarea ~a:[ a_name "Proof"; a_cols 30 ] (txt "Proof")

let proof_form =
  let open Html.D.Raw in
  Html.D.div
    ~a:[ Html.D.a_class [ "line" ]; Html.D.a_id "proof_form" ]
    [ form [ text_proof; br (); submit ] ]

let proofs_list =
  let open Html.D in
  ol ~a:[ a_id "proof_list" ] []

let proof =
  let open Html.D in
  section
    ~a:[ a_id "proof-toolbox"; a_class [ "proof" ] ]
    [
      div ~a:[ a_class [ "object" ]; a_id "source" ] [ txt "Select Source" ];
      div ~a:[ a_class [ "object" ]; a_id "target" ] [ txt "Select Target" ];
      div ~a:[ a_class [ "line" ] ] [ h3 [ txt "Paths:" ] ];
      div ~a:[ a_class [ "line" ] ] [path];
      proof_form;
      div ~a:[ a_class [ "line" ] ] [ h3 [ txt "Proofs:" ] ];
      div ~a:[ a_class [ "line" ] ] [proofs_list];
    ]
